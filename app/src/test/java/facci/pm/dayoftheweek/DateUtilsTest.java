package facci.pm.dayoftheweek;

import org.junit.Test;
import java.util.Calendar;
import facci.pm.dayoftheweek.utils.DateUtils;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DateUtilsTest {

    @Test
    public void isTheCurrentWeekNumber_ReturnsTrue() {
        Calendar calendar = Calendar.getInstance();
        DateUtils dateUtils = new DateUtils(calendar);

        assertTrue(dateUtils.isTheCurrentWeekNumber(calendar.get(
                Calendar.WEEK_OF_YEAR)));
    }

    @Test
    public void isTheCurrentWeekNumber_ReturnsFalse() {
        Calendar calendar = Calendar.getInstance();
        DateUtils dateUtils = new DateUtils(calendar);
        assertFalse(dateUtils.isTheCurrentWeekNumber(33));
    }
}