package facci.pm.dayoftheweek.utils;

import java.util.Calendar;

public class DateUtils {
    private final Calendar calendar;

    public DateUtils(Calendar calendar) {
        this.calendar = calendar;
    }

    /**
     * Return true if weekNumber param is the current week number. If not, return false.
     */
    //Metodo para prueba unitaria
    public boolean isTheCurrentWeekNumber(int weekNumber) {
        return getCurrentWeekNumber() == weekNumber;
    }

    private int getCurrentWeekNumber() {
        return calendar.get(Calendar.WEEK_OF_YEAR);
    }
}

